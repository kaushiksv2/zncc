# ZNCC Based Depthmapping with Multithreading (CPU) and OpenCL (GPU)


## Example usage on GPU:
```
      $ ./zncc --use-gpu
      la 26.5.2018 10.32.04 +0300 ::  maxdisp = 64;  winsize = 09;  thres = 08;  nhood = 08, t_sg = 0.0000 ms;  t_d0 = 38.0000 ms;  t_d1 = 38.0000 ms;  t_cc = 0.0000 ms;  t_of = 0.0000 ms

      $ ./zncc -g -w 19 -t 25 --show-status
      Reading png files...
      Shrink/grey...
      Computing depthmap 1 of 2
      Computing depthmap 2 of 2
      Cross checking...
      Occlusion fill...
      Copying to host memory....
      Done.
      Wed May 30 18:49:35 EEST 2018 ::  maxdisp = 64;  winsize = 19;  thres = 25;  nhood = 08, t_sg = 0.0000 ms;  t_d0 = 335.0000 ms;  t_d1 = 335.0000 ms;  t_cc = 0.0000 ms;  t_of = 2.0000 ms
      
      $ ./zncc --use-gpu --platform-number=0 --device-number=1
      No device or invalid device number. Aborting...
      Aborted (core dumped)

      $ ./zncc --use-gpu --image-0="path/to/im0.png"
      Error: 78
      failed to open file for reading
      Aborted (core dumped)

```

## Example usage on CPU:
```
      $ ./zncc --nthreads=25
      $ ./zncc -j 20 --image-0="path/to/im0.png"
      $ ./zncc --nthreads=20 -w 9 --show-status
```

## Compilation:
```
      ## Ensure paths are ok

      $ bash
      $ export LIBRARY_PATH="/usr/local/cuda-8.0/targets/x86_64-linux/lib"
      $ export CPLUS_INCLUDE_PATH="/usr/local/cuda-8.0/targets/x86_64-linux/include"

      ## CPU ONLY VERSION:
      $ make cpu
      $ ./zncc --use-gpu
      Recompile with GPU support :)

      ## WITH GPU SUPPORT:
      $ make
      $ ./zncc --use-gpu
      la 26.5.2018 10.42.28 +0300 ::  maxdisp = 64;  winsize = 09;  thres = 08;  nhood = 08, t_sg = 0.0000 ms;  t_d0 = 39.0000 ms;  t_d1 = 39.0000 ms;  t_cc = 0.0000 ms;  t_of = 0.0000 ms
```


im0.png and im1.png are used by default. Every call will append a line to performance_log.txt with timestamp, parameters, and timing information. In CPU mode, set the shell variable INTIMG=1 to get preliminary depthmaps before cross-checking and occlusion filling. Try "zncc --help" for more available options.


## Options

	  -h, --help                    Print help and exit
	  -V, --version                 Print version and exit
	  -w, --window-size=INT         Side of the window used for zncc. Must be odd.
	                                  (Ex: 11, window has 121 elements)
	                                  (default=`9')
	  -t, --threshold=INT           The threshold used for cross-checking.
	                                  (default=`8')
	  -n, --neighbourhood-size=INT  The neighbourhood size for occlusion filling.
	                                  (default=`8')
	  -d, --maximum-disparity=INT   The maximum disparity between images.
	                                  (default=`64')
	  -g, --use-gpu                 Use GPU for computation.  (default=off)
	  -j, --nthreads=INT            Number of threads for zncc computation. Has no
	                                  effect when using GPU.  (default=`1')
	  -q, --query-gpu-info          Query the GPU for:
	                                          CL_DEVICE_LOCAL_MEM_TYPE
	                                          CL_DEVICE_LOCAL_MEM_SIZE
	                                          CL_DEVICE_MAX_COMPUTE_UNITS
	                                          CL_DEVICE_MAX_CLOCK_FREQUENCY
	                                          CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE
	                                          CL_DEVICE_MAX_WORK_GROUP_SIZE
	                                          CL_DEVICE_MAX_WORK_ITEM_SIZES
	                                  (default=off)
	      --show-status             Print status-update messages that describe the
	                                  ongoing activity.  (default=off)
	      --platform-number=INT     The platform number (different from platform
	                                  ID) varies from 0..N_PLATFORMS-1. Use a tool
	                                  like clinfo to customize this.  (default=`0')
	      --device-number=INT       The device number (different from device ID)
	                                  varies from 0..N_DEVICES-1. Use a tool like
	                                  clinfo to customize this.  (default=`0')
	  -s, --skip-depthmapping       OBSELETE. Previously, this flag had been used
	                                  to skip computation of preliminary depthmaps,
	                                  and reuse previously output images. Has no
	                                  effect when using GPU. This option would use
	                                  images specified by --image-0 and --image-1
	                                  options. if ommitted, it looks for previously
	                                  output files at ./outputs/ directory, and use
	                                  them to perform just cross-checking and
	                                  occlusion-filling. Missing files would cause
	                                  the program to terminate. `d0_filepath` and
	                                  `d1_filepath` in zncc.cpp define the default
	                                  files that would be looked for.
	                                  (default=off)
	      --image-0=STRING          Image 0 filepath  (default=`im0.png')
	      --image-1=STRING          Image 1 filepath  (default=`im1.png')
	      --shrink-by=INT           Shrink factor to downscale image. Typically set
	                                  to 1 when skipping depthmapping step.
	                                  (default=`4')

	In CPU mode, set shell variable INTIMG=1 in order to output intermediary images
	to 'outputs/' directory.

## LICENSE
See LICENSE.md
