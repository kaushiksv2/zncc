#include "includes.h"

int update_status_b = 0;

unsigned char * read_file(const char * const filename) {
    FILE *f = fopen(filename, "r");
    if(!f) return NULL;
    fseek(f, 0, SEEK_END);
    long length = ftell(f);
    fseek(f, 0, SEEK_SET);
    unsigned char *buffer = (unsigned char *)malloc(length + 1);
    if(!buffer) return NULL;
    buffer[length] = '\0';
    fread(buffer, 1, length, f);
    fclose(f);
    return buffer;
}

void pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data)
{
    fprintf(stderr, "OpenCL Error (via pfn_notify): %s\n", errinfo);
}

void handle_lodepng_error(int error){
    if (error) {
        char s[128];
        sprintf_s(s, 128, "%d\n%s", error, lodepng_error_text(error));
        std::cout<<"Error: "<<s<<std::endl;
        abort();
    }
}

void calc_elapsed_times (struct timeval *t, double *elapsedTimes, int t_n){
    for(int i=0; i<t_n; i+=2){
        if (t[i] == t[i+1]) {
            elapsedTimes[i>>1] = 0;
        } else{
            elapsedTimes[i>>1] = (t[i+1].tv_sec - t[i].tv_sec) * 1000.0;
            elapsedTimes[i>>1] += (t[i+1].tv_usec - t[i].tv_usec) / 1000.0;
        }
    }
}

#ifdef GPU_SUPPORT
#define DOTS " ..................................."
#define CL_PRINT_DEVICE_INFO_NUMERIC(param_name, value_type, suffix_str)                    \
    ({                                                                                      \
        CL_CHECK(clGetDeviceInfo(device, param_name, sizeof(ret), (void *)ret, &ret_size)); \
        value_type value = *((value_type *)ret);                                            \
        printf("%.40s : %ld %s\n", #param_name DOTS, (long int)value, suffix_str);          \
    })

void query_gpu  (   const int platform_number,
                    const int device_number ) {

    using namespace cl_objects;

    CL_CHECK(clGetPlatformIDs(100, platforms, &platforms_n));
    if (platforms_n == 0) {
        puts("No platform. Aborting...");
        abort();
    }
    CL_CHECK(clGetDeviceIDs(platforms[platform_number], CL_DEVICE_TYPE_GPU, 100, devices, &devices_n));
    if (devices_n == 0 || !in_range(device_number, 0, devices_n-1)) {
        puts("No device or invalid device number. Aborting...");
        abort();
    }
    device = devices[device_number];

    char ret[32];
    size_t ret_size;

    /* Print local memory type */
    cl_device_local_mem_type mem_type;
    CL_CHECK(clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_TYPE, sizeof(mem_type), &mem_type, &ret_size));
    printf("%.40s : %s\n", "CL_DEVICE_LOCAL_MEM_TYPE" DOTS, mem_type==CL_GLOBAL?"CL_GLOBAL":"CL_LOCAL");

    /* Print Infos with numeric values */
    CL_PRINT_DEVICE_INFO_NUMERIC (CL_DEVICE_LOCAL_MEM_SIZE, cl_ulong, "Bytes");
    CL_PRINT_DEVICE_INFO_NUMERIC (CL_DEVICE_MAX_COMPUTE_UNITS, cl_uint, "");
    CL_PRINT_DEVICE_INFO_NUMERIC (CL_DEVICE_MAX_CLOCK_FREQUENCY, cl_uint, "MHz");
    CL_PRINT_DEVICE_INFO_NUMERIC (CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, cl_ulong, "Bytes");
    CL_PRINT_DEVICE_INFO_NUMERIC (CL_DEVICE_MAX_WORK_GROUP_SIZE, size_t, "Bytes");

    /* Print max work-item dimensions, and size along each dimension */
    CL_PRINT_DEVICE_INFO_NUMERIC (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, cl_uint, "");
    int n_dimensions = *((cl_uint *)ret);
    CL_CHECK(clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(ret), ret, &ret_size));
    printf("%.40s : ", "CL_DEVICE_MAX_WORK_ITEM_SIZES" DOTS);
    for(int i=0; i<n_dimensions; i++)
        printf("%d%c", ((size_t *)(ret))[i], (i+1-n_dimensions)?'x':'\n');
}

void cl_decode_error(cl_int enumber){
    char *s = (char *)calloc(102400);
    // Defined in clerrmacros.h
    FIND_STRINGIFY_APPEND_ALL_CL_ERRORS 
    puts(s+2);
    free(s);
}
#endif